# __Math.book__

## Introduzione

Math.book è una repository contenente materiale didattico del corso di
Algebra Lineare e Geometria Analitica alla facoltà di Informatica
dell'Università degli Studi di Milano Bicocca.
Il materiale è scritto in \LaTeX, in modo creare della documentazione
pulita.

## __Struttura della repository

La repository è organizzata in due aree:

* src (la radice dei sorgenti)
	* riassunto
	* esercizi
	* materiale aggiuntivo
* output(la radice dei contenuti finali solitamente in pdf)
	* riassunto
	* esercizi
	* materiale aggiuntivo


